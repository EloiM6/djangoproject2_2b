from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, get_object_or_404,redirect
from Prova1.models import Persona, Adressa
from django.http import Http404
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.
from django.views.generic.base import View
class Prova1(View):
    # Definim el mètode HTTP el qual s'ha d'atendre
    def get(self,request ):
        return HttpResponse(content='Això és una prova')
class Prova2(View):
    def get(self,request ):
        Adresanova = Adressa.objects.create(Nom="Adressa1", TipusVia = 1)
        Persona.objects.create(Nom="Prova1", Cognoms = "Prova11", DNI="12345678J", Genere=1, Adressa=Adresanova)
        #return HttpResponse(content=str(Persona.objects.count()));
        Persones = Persona.objects.last()
        return HttpResponse(content='El nom de la persona és '+ str(Persones.Nom))
class ExempleGet1(View):
    def get(self,request,Nom):
        try:
            #Get només accepta un valor
            #S'hi ha més d'un valor, falla.
            #Millor per id
            persona = Persona.objects.get(Nom=Nom)
        except Persona.DoesNotExist:
            raise Http404("Aquest nom no existeix")
        return HttpResponse(content='El nom de la persona és ' + str(persona.Nom))
class ExempleGet2(View):
    def get(self,request,Nom):
        # Sino troba surt un missatge 404 en comptes
        # d'una exepció de que no existeix
        persona = get_object_or_404(Persona,Nom=Nom)
        return HttpResponse(content='El nom de la persona és ' + str(persona.Nom))

class ExempleGet3(View):
    def get(self,request,id):
        try:
            persona = Persona.objects.get(id=id)
        except Persona.DoesNotExist:
            raise Http404("Aquesta persona no existeix")
        return HttpResponse(content='El nom de la persona és ' + str(persona.Nom))

class MultipleURL(View):
    # Definim el mètode HTTP el qual s'ha d'atendre
    def get(self,request ):
        return HttpResponse(content='Exemple de multiples urls agrupades')

class ExempleFilter(View):
    def get(self,request,Cognoms):
        # Filter retorna tots els registres coincidents,
        # no només un, sinó una llista
        persona = Persona.objects.filter(Cognoms=Cognoms)
        return HttpResponse(content='El nom de la persona és ' + str(persona[0].Nom))

class ExempleFilterAltraTaula(View):
    def get(self,request):
        # Exemple de com podem usar la relació
        persona = Persona.objects.filter(Adressa__TipusVia =1)
        return HttpResponse(content='El nom de la persona és ' + str(persona[1].Nom))

class ExempleFilterContains(View):
    def get(self,request):
        # Exemple de com podem usar operadors com contains o like
        persona = Persona.objects.filter(Adressa__Nom__contains = "Adres")
        return HttpResponse(content='El nom de la persona és ' + str(persona[1].Nom ))
class ExempleVista(View):
    def get(self, request):
        context = {
            'persones': list(Persona.objects.all())
        }
        return render(request, 'persones.html', context=context)

class ExempleAdressa(View):
    def get(self, request,id):
        context = {
            'adressa': Adressa.objects.get(id=id)
        }
        return render(request, 'adressa.html', context=context)

class ExempleAutenticacio(View):
    def get(self,request,id):

        if request.user.is_authenticated:
            try:
                #logout(request)
                persona = Persona.objects.get(id=id)
            except Persona.DoesNotExist:
                raise Http404("Aquesta persona no existeix")
            return HttpResponse(content='El nom de la persona és ' + str(persona.Nom))
        return redirect('login')

# Com que hereta directament de LoginRequiredMixin,
#  ja fa la comprovació de login
class ExempleAutenticacio2(LoginRequiredMixin,View):
    def get(self,request,id):
        try:
            #logout(request)
            persona = Persona.objects.get(id=id)
        except Persona.DoesNotExist:
            raise Http404("Aquesta persona no existeix")
        return HttpResponse(content='El nom de la persona és ' + str(persona.Nom))

