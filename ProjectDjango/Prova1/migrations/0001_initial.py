# Generated by Django 4.0.3 on 2022-03-28 09:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Adressa',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nom', models.CharField(default='', max_length=30)),
                ('TipusVia', models.CharField(choices=[(0, 'Carrer'), (1, 'Carretera'), (2, 'Avinguda'), (3, 'Cami')], max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nom', models.CharField(default='', max_length=30)),
                ('Cognoms', models.CharField(default='', max_length=50)),
                ('DNI', models.CharField(max_length=9, unique=True)),
                ('Genere', models.CharField(choices=[(0, 'Dona'), (1, 'Home'), (2, 'Altres'), (3, 'No binari')], max_length=30)),
                ('Adressa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Prova1.adressa')),
            ],
        ),
    ]
