import json
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from Prova1.models import Persona
from Prova1.api.serializers import PersonaSerializer

class PersonaApiView(APIView):
    def get(self, request):
        persones = [Persona.Nom for persona in Persona.objects.all()]
        persona = Persona.objects.all()[0]
        return Response(data=persona.Nom, status=status.HTTP_200_OK)
        #return Response({"message": "Hello, world!"})
class Persona2ApiView(APIView):
    def get(self, request):
        persones = PersonaSerializer(Persona.objects.all(),many=True)

        return Response(data=persones.data, status=status.HTTP_200_OK)