from rest_framework import serializers
from Prova1.models import Persona
class PersonaSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Persona
        fields = '__all__'

    '''Nom = serializers.CharField()
        DNI = serializers.CharField()'''