from django.contrib import admin
from django.contrib.admin import ModelAdmin
from Prova1.models import Persona

# Register your models here.

class PersonaAdmin(ModelAdmin):
    pass

admin.site.register(Persona,ModelAdmin)