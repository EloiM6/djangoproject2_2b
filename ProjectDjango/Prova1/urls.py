from django.urls import path,include

from . import views
from Prova1.views import MultipleURL,ExempleFilter,ExempleFilterAltraTaula,ExempleFilterContains,ExempleVista,ExempleAdressa
from Prova1.api.views import PersonaApiView,Persona2ApiView
app_name = 'multipleurl'
urlpatterns = [
    path('',MultipleURL.as_view()),
    path('filter/<Cognoms>',ExempleFilter.as_view()),
    path('ExempleFilterAltraTaula/',ExempleFilterAltraTaula.as_view()),
    path('ExempleFilterContains/',ExempleFilterContains.as_view()),
    path('ExempleVista/', ExempleVista.as_view()),
    path('adressa/<id>', ExempleAdressa.as_view()),
    path('llistapersones', PersonaApiView.as_view()),
    path('llistapersones2', Persona2ApiView.as_view())
]
