"""ProjectDjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from Prova1.views import Prova1, Prova2, ExempleGet1,ExempleGet2,ExempleGet3,ExempleAutenticacio,ExempleAutenticacio2
from users.views import LoginView,LogoutView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',Prova1.as_view()),
    path('Prova2/',Prova2.as_view()),
    path('Exempleget1/<Nom>/', ExempleGet1.as_view()),
    path('Exempleget2/<Nom>/', ExempleGet2.as_view()),
    path('Exempleget3/<int:id>/', ExempleGet3.as_view()),
    path('multipleurl/', include('Prova1.urls')),
    path('login/', LoginView.as_view(), name='login'),
    path('ExempleAutenticacio/<int:id>', ExempleAutenticacio.as_view()),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('ExempleAutenticacio/<int:id>', ExempleAutenticacio2.as_view()),
]
